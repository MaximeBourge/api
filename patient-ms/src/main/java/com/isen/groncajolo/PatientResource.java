package com.isen.groncajolo;

import org.bson.types.ObjectId;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.validation.constraints.Null;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import java.util.List;

@Path("/patients")
public class PatientResource {

    @Inject
    PatientRepository patientRepository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<PatientEntity> getPatients() {
        return patientRepository.findAll().list();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public PatientEntity getPatient(@PathParam("id") ObjectId id) {
        return patientRepository.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPatient(PatientEntity newPatient){
        if(newPatient != null){
            patientRepository.persist(newPatient);   
            return Response.status(Status.CREATED).build(); 
        }
        return Response.status(Status.BAD_REQUEST).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePatient(@PathParam("id") ObjectId id,PatientEntity newPatient){
        if(newPatient != null && id != null){
            newPatient.id = id;
            patientRepository.update(newPatient);
            return Response.status(Status.NO_CONTENT).build(); 
        }
        return Response.status(Status.BAD_REQUEST).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePatient(@PathParam("id") ObjectId id){
        if(id != null){
            patientRepository.deleteById(id);
            return Response.status(Status.NO_CONTENT).build(); 
        }
        return Response.status(Status.BAD_REQUEST).build();
    }    
}
